package main

import "fmt"

func main() {
	ch := make(chan int, 1)

	ch <- 1 // escreve 1 no canal
	<-ch    // lê dados do canal (mas não armazena em lugar nenhum)

	ch <- 2
	fmt.Println(<-ch)
}
