package main

import (
	"fmt"
	"time"
)

func falar(pessoa, texto string, qtd int) {
	for i := 0; i < qtd; i++ {
		time.Sleep(time.Second)
		fmt.Printf("%s : %s x %d\n", pessoa, texto, i+1)
	}
}

func main() {
	go falar("Fulano", "opa!", 10)
	falar("Bertrano", "Iaew!", 5)
	fmt.Println("As goroutines precisam que a thread principal estejam \"executando\"")
}
