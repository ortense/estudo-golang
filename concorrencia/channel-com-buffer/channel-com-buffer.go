package main

import (
	"fmt"
	"time"
)

func routine(ch chan int) {
	ch <- 1
	ch <- 2
	ch <- 3
	fmt.Println("Executando rotina")
	ch <- 4
	ch <- 5
	fmt.Println("Isso aqui não vai ser executado pq não tem mais espaço no buffer")
	ch <- 6
}

func main() {
	ch := make(chan int, 3)
	go routine(ch)

	time.Sleep(time.Second)
	fmt.Println(<-ch)
}
