package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
)

// Google I/O 2012 = Co concurrency Patterns

// <-chan // isso cria um canal somente leitura

func titulo(urls ...string) <-chan string {
	c := make(chan string)

	for _, url := range urls {
		go func(url string) {
			res, _ := http.Get(url)
			html, _ := ioutil.ReadAll(res.Body)
			r, _ := regexp.Compile("<title>(.*?)<\\/title>")
			c <- r.FindStringSubmatch(string(html))[1]
		}(url)
	}

	return c
}

func main() {
	t1 := titulo("https://www.google.com", "https://www.youtube.com/")
	t2 := titulo("https://twitter.com/", "https://golang.org/")
	fmt.Println("Primeiro:", <-t1, " | ", <-t2)
	fmt.Println("segundo:", <-t1, " | ", <-t2)
}
