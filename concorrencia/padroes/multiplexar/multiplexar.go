package main

import (
	"fmt"

	"github.com/cod3rcursos/html"
)

func encaminhar(origem <-chan string, destino chan string) {
	for {
		destino <- <-origem
	}
}

func juntar(entrada1, entrada2 <-chan string) <-chan string {
	c := make(chan string)
	go encaminhar(entrada1, c)
	go encaminhar(entrada2, c)
	return c
}

func main() {
	c := juntar(
		html.Titulo("https://www.google.com", "https://www.youtube.com/"),
		html.Titulo("https://twitter.com/", "https://golang.org/"),
	)

	fmt.Println(<-c, "\n", <-c, "\n", <-c, "\n", <-c)
}
