package main

import (
	"fmt"
	"time"
)

func falar(pessoa string) <-chan string {
	c := make(chan string)
	go func() {
		for i := 0; i < 3; i++ {
			time.Sleep(time.Second)
			c <- fmt.Sprintf("%s falando: %d", pessoa, i+1)
		}
	}()
	return c
}

func join(c1, c2 <-chan string) <-chan string {
	c := make(chan string)
	go func() {
		for {
			select {
			case s := <-c1:
				c <- s
			case s := <-c2:
				c <- s
			}
		}
	}()
	return c
}

func main() {
	c := join(
		falar("Fulano"),
		join(
			falar("Beltrano"),
			falar("Ciclano"),
		),
	)

	fmt.Println(<-c, <-c, <-c)
	fmt.Println(<-c, <-c, <-c)
	fmt.Println(<-c, <-c, <-c)
}
