package main

import (
	"fmt"
	"time"
)

func rotina(c chan int) {
	time.Sleep(time.Second)
	c <- 1
	fmt.Println("Canal lido")
}

func main() {
	c := make(chan int)

	go rotina(c)

	fmt.Println(<-c) // operação bloqueante
	fmt.Println("lido")
	fmt.Println(<-c) //deadlock
	fmt.Println("FIM")
}
