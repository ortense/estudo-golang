package main

import (
	"fmt"
	"time"
)

func doisTresQuatroVezes(base int, ch chan int) {
	time.Sleep(time.Second)

	ch <- base * 2

	time.Sleep(time.Second)

	ch <- base * 3

	time.Sleep(time.Second * 2)

	ch <- base * 4
}

func main() {
	ch := make(chan int)
	go doisTresQuatroVezes(2, ch)

	a, b := <-ch, <-ch

	fmt.Println(a, b)
	fmt.Println(<-ch)

	fmt.Println("Canais são síncronos!!")
}
