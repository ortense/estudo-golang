package main

func main() {
	// Esse arquivo tem visibilidade de tudo que foi definido no pacote main
	// sem a necessidade de imports
	print(sum(2, 3))
}
