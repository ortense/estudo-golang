package main

import (
	"fmt"
	"math"
)

func main() {
	a := 3
	b := 2

	fmt.Println("soma", a+b)
	fmt.Println("subitração", a-b)
	fmt.Println("divisão", a/b)
	fmt.Println("multiplicação", a*b)
	fmt.Println("módulo", a%b)

	// bitwise

	fmt.Println("AND", a&b)
	fmt.Println("OR", a|b)
	fmt.Println("XOR", a^b)

	// Math

	c := 3.0
	d := 2.0

	fmt.Println("Maior", math.Max(c, d))
	fmt.Println("Menor", math.Min(c, d))
	fmt.Println("Potência", math.Pow(c, d))

	fmt.Println("O pacote math possui outros métodos")
}
