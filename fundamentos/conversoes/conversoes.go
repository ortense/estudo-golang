package main

import (
	"fmt"
	"strconv"
)

func main() {
	fmt.Println("Convertendo números")

	x := 3.1416
	y := 2
	z := 99.9

	fmt.Println(x/float64(y), int(z))
	fmt.Println("string(n) converte \"para rune\"")
	fmt.Println("string(97) =", string(924))

	fmt.Println("Use strconv.Itoa para converter de número para string")

	s := strconv.Itoa(42)

	fmt.Printf("strconv.Itoa(42) = %q\n", s)

	fmt.Println("strconv.Atoi converte string para intero")
	fmt.Println("strconv.Atoi retorna o valor e um erro")

	num, err := strconv.Atoi("123")
	fmt.Println("num, err := strconv.Atoi(\"123\")")
	fmt.Println("num =", num)
	fmt.Println("err =", err)

	fmt.Println("O pacote strconv também possui outros métodos - dar uma olhada")
}
