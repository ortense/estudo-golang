package main

import "fmt"

func main() {
	i := 1

	var p *int // definição do ponteiro

	p = &i // associação do ponteiro p ao endereço de memoria da variavável i

	*p++
	i++

	fmt.Println("Endereço de memória", p, &i)
	fmt.Println("Valor", *p, i)
	fmt.Println("Go não tem aritmética de ponteiros") // p++
}
