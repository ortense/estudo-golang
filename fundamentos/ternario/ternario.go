package main

import "fmt"

// Esse tipo de coisa pode dar ruin...
func when(condition bool, whenTrue, whenFalse interface{}) interface{} {
	if condition {
		return whenTrue
	}
	return whenFalse
}

func main() {
	fmt.Println("Go não tem ternário :O")
	r := when(10 > 100, "verdade", false)
	fmt.Println(r)
}
