package main

import (
	"fmt"
)

func main() {
	fmt.Print("fmt.Print escreve ")
	fmt.Print("na mesma linha")
	fmt.Println(".")
	fmt.Println("fmt.Println pula a linha depois de cada execução")

	const PI = 3.141516
	fmt.Println("fmt.Sprint converte um valor qualquer em string")
	fmt.Println("Valor de PI = " + fmt.Sprint(PI))

	fmt.Println("fmt.Printf imprime formatado")
	fmt.Printf("PI = %.2f\n", PI)
	fmt.Println("Existm vários \"ganchos\" para cada tipo de dado e formatação")
	fmt.Println("https://gobyexample.com/string-formatting")
}
