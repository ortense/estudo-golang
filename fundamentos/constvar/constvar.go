package main

import (
	"fmt"
	"math"
)

func main() {

	// muitas formas de declarar constantes e variáveis

	const PI float64 = 3.1415
	const raio = 3.2

	area := PI * math.Pow(raio, 2)

	fmt.Println("area", area)

	const (
		a = 1
		b = 2
	)

	var (
		c = 3
		d = 4
	)

	fmt.Println(a, b, c, d)

	var x, y = "hmmm", 2
	w, z := true, false

	fmt.Println(x, y, w, z)
}
