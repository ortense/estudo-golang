package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println("String:", "teste" == "teste")
	fmt.Println("!=", 3 != 1)
	fmt.Println("<", 3 < 2)
	fmt.Println(">", 3 > 2)
	fmt.Println("<=", 3 <= 2)
	fmt.Println(">=", 3 >= 2)

	d1 := time.Unix(0, 0)
	d2 := time.Unix(0, 0)
	fmt.Println("datas", d1 == d2)
	fmt.Println("datas", d1.Equal(d2))

	type Pessoa struct {
		Nome string
	}

	p1 := Pessoa{"Marcus"}
	p2 := Pessoa{"Marcus"}
	fmt.Println("Structs", p1 == p2)
}
