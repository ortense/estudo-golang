package main

import (
	"fmt"
	"math"
	"reflect"
)

func main() {
	fmt.Println("# Tipos básicos em Go")
	fmt.Println("## Números")
	fmt.Println(1, 2, 100)
	fmt.Println("Literal inteiro:", reflect.TypeOf(99999))
	fmt.Println("*Conjunto dos números naturais uint8, uint16, uint32 e uint64*")

	var b byte = 255

	fmt.Println("byte é um alias para", reflect.TypeOf(b))
	fmt.Println("*Conjunto dos números inteiros int8, int16, int32 e int64*")
	fmt.Println("Valor máximo do int", math.MaxFloat64)
	fmt.Println("Tipo do maior inteiro é", reflect.TypeOf(math.MaxInt64))

	const r rune = 'a'

	fmt.Println("rune representa o valor numério da tabela unicode")
	fmt.Println("r rune = 'a' corresponde a", r)
	fmt.Println("O tipo de rune é", reflect.TypeOf(r))

	const f float32 = 49.99
	fmt.Println("*Conjunto dos números reais float32 e float64*")
	fmt.Println("tipo para f float32 = 49.99", reflect.TypeOf(f))
	fmt.Println("Tipo do literal de 49.99", reflect.TypeOf(49.99))

	const x = float64(99.999)
	fmt.Println(reflect.TypeOf(x), x)

	fmt.Println("## Boleanos")

	b1 := true
	const b2 bool = false
	fmt.Println(b1, "&", b2, reflect.TypeOf(b1))

	fmt.Println("## Strings")

	const s string = "Strings são delimitadas por aspas duplas"
	fmt.Println(s)

	st := "Go não possui o tipo Char"
	fmt.Println(st)

	var str = `Da mesma forma que em JS a crase
é utilizada para strings
com multiplas linhas`
	fmt.Println(str)

	const l = "A função len retorna o tamanho da string"
	fmt.Println(l, "\nlen(l) =", len(l))
}
