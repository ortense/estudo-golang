package main

import (
	"fmt"

	"github.com/cod3rcursos/area"
)

func main() {
	fmt.Println(area.Circulo(6))
	fmt.Println(area.Quadrilatero(5, 2))
}
