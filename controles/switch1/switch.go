package main

import (
	"fmt"
)

func notaParaConceito(n float64) string {
	var nota = int(n)
	switch nota {
	case 10:
		fmt.Println("O comportamento padrão do switch é dar \"breack\" a cada \"case\"")
		fmt.Println("Se essse não for o comportamento desejado usar a palavra reservada \"fallthrough\"")
		fallthrough
	case 9:
		return "A"
	case 8, 7:
		return "B"
	case 6, 5:
		return "C"
	case 4, 3:
		return "D"
	case 2, 1, 0:
		return "E"
	default:
		return "Nota inválida"
	}
}

func main() {
	fmt.Println(notaParaConceito(10))
	fmt.Println(notaParaConceito(8))
	fmt.Println(notaParaConceito(6))
	fmt.Println(notaParaConceito(3))
	fmt.Println(notaParaConceito(2))
	fmt.Println(notaParaConceito(-2))
}
