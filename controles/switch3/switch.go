package main

import (
	"fmt"
	"reflect"
	"time"
)

func tipo(t interface{}) string {
	fmt.Print(reflect.TypeOf(t), ": ")

	switch t.(type) {
	case int:
		return "número inteiro"
	case float32, float64:
		return "número real"
	case string:
		return "texto"
	case func():
		return "função"
	default:
		return "tipo desconhecido"
	}
}

func main() {
	fmt.Println(tipo(1))
	fmt.Println(tipo(0.99))
	fmt.Println(tipo("loren"))
	fmt.Println(tipo(func() {}))
	fmt.Println(tipo(time.Now()))
}
