package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println("For tradicional")

	for i := 0; i <= 20; i += 2 {
		fmt.Printf("%d ", i)
	}

	fmt.Println("\nFor like while")

	i := 1

	for i <= 10 {
		fmt.Printf("%d ", i)
		i++
	}

	fmt.Println("\nLoop infinito")

	c := 1

	for {
		fmt.Println("...")

		time.Sleep(time.Second)

		c++

		if c >= 4 {
			break
		}
	}

	fmt.Println("Ainda falta ver o range que parece um forEach")
}
