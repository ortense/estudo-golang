package main

import "fmt"

type produto struct {
	nome     string
	preco    float64
	desconto float64
}

// funções com receptores (receiver) é semelhante a um método em POO
func (p produto) precoComDesconto() float64 {
	return p.preco * (1 - p.desconto)
}

func main() {
	var prod1 produto

	prod1 = produto{
		nome:     "lapis",
		preco:    1.79,
		desconto: .05,
	}

	fmt.Println(prod1, prod1.precoComDesconto())

	prod2 := produto{"Notebook", 1999.99, .1}

	fmt.Println(prod2, prod2.precoComDesconto())
}
