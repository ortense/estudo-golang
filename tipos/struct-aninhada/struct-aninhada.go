package main

import "fmt"

type item struct {
	productID uint
	qtd       uint
	preco     float64
}

type pedido struct {
	userID int
	itens  []item
}

func (p pedido) valorTotal() float64 {
	total := .0

	for _, item := range p.itens {
		total += item.preco * float64(item.qtd)
	}

	return total
}

func main() {
	pedido := pedido{
		userID: 1,
		itens: []item{
			item{productID: 1, preco: 25, qtd: 4},
			item{productID: 2, preco: 95.50, qtd: 1},
			item{productID: 3, preco: 30.3, qtd: 2},
		},
	}

	fmt.Printf("Valor total do pedido: R$ %.2f", pedido.valorTotal())
}
