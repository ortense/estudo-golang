package main

import "fmt"

type course struct {
	name string
}

func main() {
	var anyThing interface{}

	anyThing = 1
	fmt.Println(anyThing)

	anyThing = true
	fmt.Println(anyThing)

	anyThing = "booom"
	fmt.Println(anyThing)

	anyThing = course{"Golang"}
	fmt.Println(anyThing)
}
