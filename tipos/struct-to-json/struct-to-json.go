package main

import (
	"encoding/json"
	"fmt"
)

type product struct {
	ID    uint     `json:"id"`
	Name  string   `json:"name"`
	Price float64  `json:"price"`
	Tags  []string `json:"tags"`
}

func main() {
	notebook := product{
		ID:    1,
		Name:  "Pixel",
		Price: 999.99,
		Tags:  []string{"notebook", "chromebook"},
	}

	notebookJSON, _ := json.Marshal(notebook)

	fmt.Println(string(notebookJSON))

	var fromJSON product
	jsonString := `{
		"id":1,
		"name":"Pixel",
		"price":999.99,
		"tags":["notebook","chromebook"]
	}`
	json.Unmarshal([]byte(jsonString), &fromJSON)
	fmt.Println(fromJSON.ID, fromJSON.Name, fromJSON.Price, fromJSON.Tags)
}
