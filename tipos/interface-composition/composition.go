package main

import "fmt"

type sportingCar interface {
	turnTurboOn()
}

type luxuryCar interface {
	selfParking()
}

type sportingLuxuryCar interface {
	luxuryCar
	sportingCar
	// other ...
}

type bmw struct{}

func (b bmw) turnTurboOn() {
	fmt.Println("turbo!")
}

func (b bmw) selfParking() {
	fmt.Println("parking...")
}

func main() {
	var b sportingLuxuryCar = bmw{}

	b.turnTurboOn()
	b.selfParking()
}
