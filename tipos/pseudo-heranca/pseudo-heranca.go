package main

import "fmt"

type car struct {
	name         string
	currentSpeed uint
}

type ferrari struct {
	car
	turboOn bool
}

func main() {
	f := ferrari{}
	f.name = "F40"
	f.currentSpeed = 0
	f.turboOn = false

	fmt.Printf("ferrari name %s \nturno on: %v\n", f.name, f.turboOn)
	fmt.Println(f)
}
