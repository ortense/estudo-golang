package main

import "fmt"

// Nota é um tipo personalizado criado com base no float64
type Nota float64

/*
 * Remover o comentário da declaração de tipo gera um warning
 * "Exported type Nota should have a comment or be unexported"
 * Isso se deve a uma convenção referente a Go Doc Comments
 * https://github.com/golang/lint/issues/191
 */

func (nota Nota) entre(n1, n2 float64) bool {
	n := float64(nota)
	return n >= n1 && n <= n2
}

func (nota Nota) obterConceito() string {
	switch {
	case nota.entre(9, 10):
		return "A"
	case nota.entre(7, 8.99):
		return "B"
	case nota.entre(5, 7.99):
		return "C"
	case nota.entre(3, 4.99):
		return "D"
	case nota.entre(1, 3.99):
		return "E"
	default:
		return "F"
	}
}

func main() {
	nota := Nota(7)
	fmt.Println(nota.obterConceito())
}
