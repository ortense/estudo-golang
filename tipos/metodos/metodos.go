package main

import (
	"fmt"
	"strings"
)

type person struct {
	name     string
	lastname string
}

func (p person) getFullName() string {
	return p.name + " " + p.lastname
}

func (p *person) setFullName(fullName string) {
	splited := strings.Split(fullName, " ")
	p.name = splited[0]
	p.lastname = splited[1]
}

func main() {
	people := person{"Fulado", "da Silva"}
	fmt.Println(people.getFullName())

	people.setFullName("Beltrado Pereira")
	fmt.Println(people.getFullName())
}
