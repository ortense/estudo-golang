package main

import "fmt"

type printable interface {
	toString() string
}

type person struct {
	firstName string
	lastName  string
}

type product struct {
	name  string
	price float64
}

func (p person) toString() string {
	return p.firstName + " " + p.lastName
}

func (p product) toString() string {
	return fmt.Sprintf("%s - R$ %.2f", p.name, p.price)
}

func print(p printable) {
	fmt.Println(p.toString())
}

func main() {
	fmt.Println("Interfaces em GO são implementadas de maneira IMPLICITA")
	var p printable

	p = person{firstName: "Fulano", lastName: "da Silva"}
	print(p)

	p = product{name: "notebook", price: 1999.99}
	print(p)

	people := person{firstName: "Beltrano", lastName: "Pereira"}
	print(people)

	gift := product{"Book", 99.50}
	print(gift)
}
