package main

import (
	"fmt"
)

type sportingCar interface {
	turnOnTurbo()
}

type ferrari struct {
	modelo          string
	turboOn         bool
	velocidadeAtual uint
}

func (f *ferrari) turnOnTurbo() {
	f.turboOn = true
}

func main() {
	c1 := ferrari{"F40", false, 0}
	c1.turnOnTurbo()

	fmt.Println("Interfaces que implementam métodos que modificam uma struct devem recever um ponteiro")
	var c2 sportingCar = &ferrari{"F40", false, 10}
	c2.turnOnTurbo()

	fmt.Println(c1, c2)
}
