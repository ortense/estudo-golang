package main

import (
	"fmt"
	"log"
	"net/http"
	"time"
)

func horaAtual(w http.ResponseWriter, r *http.Request) {
	s := time.Now().Format("02/01/2006 03:04:05")
	fmt.Fprintf(w, "<h1>Hora atual: %s</h1>", s)
}

func main() {
	http.HandleFunc("/hora", horaAtual)
	log.Println("Server up at port 3000")
	log.Fatal(http.ListenAndServe(":3000", nil))
}
