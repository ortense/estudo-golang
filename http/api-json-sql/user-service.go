package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"

	_ "github.com/go-sql-driver/mysql"
)

// User estrutura que pode ser convertida para json de resposta
type User struct {
	ID   uint   `json:"id"`
	Name string `json:"name"`
}

// UserHandler manipula alguma coisa de usuário
func UserHandler(w http.ResponseWriter, r *http.Request) {
	id, _ := strconv.Atoi(strings.TrimPrefix(r.URL.Path, "/users/"))

	if r.Method == "GET" {
		if id > 0 {
			getUsersByID(w, r, id)
			return
		}
		findAllUsers(w, r)
		return
	}

	NotFoundHandler(w, r)
}

func getUsersByID(w http.ResponseWriter, r *http.Request, id int) {
	db, err := sql.Open("mysql", "root:root@/cursogo")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	var user User
	query := "select id, nome from usuarios where id = ?"

	db.QueryRow(query, id).Scan(&user.ID, &user.Name)

	jsonUser, _ := json.Marshal(user)

	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, string(jsonUser))
}

func findAllUsers(w http.ResponseWriter, r *http.Request) {
	db, err := sql.Open("mysql", "root:root@/cursogo")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	var users []User

	query := "select id, nome from usuarios"

	rows, _ := db.Query(query)
	defer rows.Close()

	for rows.Next() {
		var user User
		rows.Scan(&user.ID, &user.Name)
		users = append(users, user)
	}

	jsonUsers, _ := json.Marshal(users)

	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, string(jsonUsers))
}

func NotFoundHandler(w http.ResponseWriter, r *http.Request) {
	statusText := http.StatusText(http.StatusNotFound)
	responseError := fmt.Sprintf("{ \"error\": \"%s\" }", statusText)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusNotFound)
	fmt.Fprintf(w, responseError)
}

func main() {
	http.HandleFunc("/", NotFoundHandler)
	http.HandleFunc("/users/", UserHandler)

	log.Println("Server up!")
	log.Fatal(http.ListenAndServe(":3000", nil))
}
