package main

import (
	"database/sql"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

func main() {
	db, err := sql.Open("mysql", "root:root@/cursogo")

	if err != nil {
		panic(err)
	}

	defer db.Close()

	transaction, _ := db.Begin()
	stmt, _ := transaction.Prepare("insert into usuarios(id, nome) values(?, ?)")

	stmt.Exec(500, "Tony")
	stmt.Exec(501, "Steve")

	_, err = stmt.Exec(1, "Clint") // erro de chave duplicada

	if err != nil {
		transaction.Rollback()
		log.Fatal(err)
	}

	transaction.Commit()

}
