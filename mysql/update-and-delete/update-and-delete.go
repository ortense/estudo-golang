package main

import (
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
)

func main() {
	db, err := sql.Open("mysql", "root:root@/cursogo")

	if err != nil {
		panic(err)
	}

	defer db.Close()

	updateUser, _ := db.Prepare("update usuarios set nome = ? where id = ?")
	deleteUser, _ := db.Prepare("delete from usuarios where id = ?")

	updateUser.Exec("Antony Edward Stark", 500)
	updateUser.Exec("Steven Grant Rogers", 501)
	deleteUser.Exec(2000)
}
