package main

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
)

// func exec(db *sql.DB, sql string) sql.Result {
// 	result, err := db.Exec(sql)

// 	if err != nil {
// 		panic(err)
// 	}

// 	return result
// }

func main() {
	db, err := sql.Open("mysql", "root:root@/cursogo")

	if err != nil {
		panic(err)
	}

	defer db.Close()

	stmt, _ := db.Prepare("insert into usuarios(nome) values(?)")

	stmt.Exec("Fulano")
	stmt.Exec("Beltrano")

	res, _ := stmt.Exec("Siclano")

	id, _ := res.LastInsertId()

	fmt.Println("ultimo id criado", id)

	linhas, _ := res.RowsAffected()
	fmt.Println("linhas afetadas:", linhas)
}
