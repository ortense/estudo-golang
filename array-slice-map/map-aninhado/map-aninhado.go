package main

import (
	"fmt"
)

func main() {
	heroisPorGrupo := map[string]map[string]string{
		"Vingadores": {
			"Homem de Ferro":  "Tony Stark",
			"Capitão América": "Steve Rogers",
			"Thor":            "Thor Odynson",
			"Hulk":            "Bruce Banner",
		},
		"X-Men": {
			"Wolverine":  "Logan",
			"Ciclope":    "Scott Summers",
			"Tempestade": "Ororo Munroe",
		},
	}

	for grupo, integrantes := range heroisPorGrupo {
		fmt.Println(grupo)
		for nome, alterego := range integrantes {
			fmt.Println(nome, "/", alterego)
		}
	}
}
