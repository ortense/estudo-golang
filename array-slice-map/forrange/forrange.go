package main

import (
	"fmt"
)

func main() {
	numeros := [...]int{1, 2, 3, 4, 5}

	for index, item := range numeros {
		fmt.Printf("array[%d] = %d\n", index, item)
	}

	for _, numero := range numeros {
		fmt.Println(numero)
	}
}
