package main

import (
	"fmt"
)

func main() {
	fmt.Println("Mapas devem ser inicializados")

	mapa := make(map[int]string)

	mapa[1234] = "Fulano"
	mapa[1111] = "Bertrano"
	mapa[5562] = "Ciclano"

	for key, value := range mapa {
		fmt.Printf("chave = %d / valor = %s\n", key, value)
	}

	delete(mapa, 1111)
	fmt.Println("delete(mapa, 1111)")
	fmt.Println(mapa)
}
