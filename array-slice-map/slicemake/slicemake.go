package main

import (
	"fmt"
)

func main() {
	fmt.Println("Criar um slice de 10 elementos")
	s := make([]int, 10)

	s[9] = 12

	fmt.Println(s)

	fmt.Println("Cria um slice de tamanho 10 e capacidade do array interno de 20 itens")
	s = make([]int, 10, 20)
	fmt.Println(s, len(s), cap(s))

	fmt.Println("Adicionando itens no slice")
	s = append(s, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0)
	fmt.Println(s, len(s), cap(s))

	fmt.Println("Adicionando itens alem da capacidade do slice.")
	s = append(s, 1)
	fmt.Println(s, len(s), cap(s))
}
