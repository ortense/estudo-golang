package main

import (
	"fmt"
	"reflect"
)

func main() {
	fmt.Println("Slices são semelhantes a arrays porem com tamanho dinâmico.")

	a1 := [3]int{1, 2, 3}
	s1 := []int{1, 2, 3}

	fmt.Println(a1, s1)
	fmt.Println(reflect.TypeOf(a1), reflect.TypeOf(s1))

	fmt.Println("\nSlices também podem ser criados a partir de outros arrays ou slices")
	a2 := [5]int{1, 2, 3, 4, 5}
	s2 := a2[1:3]

	fmt.Println(a2, s2)

	s3 := a2[:2]
	fmt.Println(a2, s3)

	s4 := s2[:1]
	fmt.Println(s2, s4)

	fmt.Println("Slices não são arrays, eles possuem referências / ponteiros de outros arrays internos.")
}
