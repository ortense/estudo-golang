package main

import (
	"fmt"
)

func main() {
	fmt.Println("Declaração de map literal")
	funcionariosSalario := map[string]float64{
		"Fulano":   11111.30,
		"Bertrano": 12000.505,
	}

	funcionariosSalario["Ciclano"] = 7000

	fmt.Println(funcionariosSalario)

	fmt.Println("Deletar um elemento inexistente do map não causa erro")
	delete(funcionariosSalario, "inexistente")

	for nome, salario := range funcionariosSalario {
		fmt.Printf("%s ganha %.2f\n", nome, salario)
	}
}
