package main

import "fmt"

func obterValorAprovado(numero int) int {
	defer fmt.Println("acabou!")
	if numero > 5000 {
		fmt.Println("valor alto")
		return 5000
	}

	fmt.Println("Valor baixo")
	return numero
}

func main() {
	fmt.Println(obterValorAprovado(9000))
	fmt.Println("----------------------")
	fmt.Println(obterValorAprovado(2000))
}
