package main

import "fmt"

func troca(p1, p2 int) (segundo, primeiro int) {
	primeiro = p1
	segundo = p2

	fmt.Println("Em funções com retorno nomeado não é necessário informar as valores no \"return\"")
	return // o tal do retorno limpo ...
}

func main() {
	x, y := troca(2, 3)
	fmt.Println(x, y)
}
