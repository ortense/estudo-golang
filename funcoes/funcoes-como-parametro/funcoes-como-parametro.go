package main

import "fmt"

func sum(a, b int) int {
	return a + b
}

func sub(a, b int) int {
	return a - b
}

func div(a, b int) int {
	return a / b
}

func mult(a, b int) int {
	return a * b
}

func exec(fn func(int, int) int, x, y int) int {
	return fn(x, y)
}

func main() {
	fmt.Println(exec(sum, 6, 2))
	fmt.Println(exec(sub, 6, 2))
	fmt.Println(exec(div, 6, 2))
	fmt.Println(exec(mult, 6, 2))
}
