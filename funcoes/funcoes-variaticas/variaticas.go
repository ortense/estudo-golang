package main

import "fmt"

func media(numeros ...float64) float64 {
	total := .0

	for _, n := range numeros {
		total += n
	}

	return total / float64(len(numeros))
}

func main() {
	fmt.Printf("Média = %.2f\n", media(7.6, 8.1))
	fmt.Printf("Média = %.2f\n", media(7.1, 3, 9.5, 6.6))
	fmt.Printf("Média = %.2f\n", media()) //Not a Number Ç_ç
}
