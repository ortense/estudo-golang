package main

import "fmt"

// passagem de parâmetro por cópia de valor
func incrementar(n int) {
	n++
}

// passagem de parâmetro por referência
func incrementarPonteiro(n *int) {
	*n++
}

func main() {
	n := 1
	incrementar(n)
	fmt.Println(n)

	incrementarPonteiro(&n) //gerando efeito colateral
	fmt.Println(n)

	incrementar(n)
	fmt.Println(n)
}
