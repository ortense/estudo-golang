package main

import (
	"fmt"
)

func imprimirAprovados(aprovados ...string) {
	fmt.Println("Lista de aprovados")
	fmt.Println("------------------")

	for index, aprovado := range aprovados {
		fmt.Printf("%d) %s\n", index+1, aprovado)
	}
	fmt.Println("------------------")
}

func main() {
	aprovados := []string{"Fulano", "Ciclado", "Bertrano"}
	imprimirAprovados(aprovados...)
	fmt.Println("Listas para funções variátias só funciona com slices!!")
}
