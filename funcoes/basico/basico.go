package main

import "fmt"

func f1() {
	fmt.Println("F1 não recebe parâmetro e não retorna nada")
}

func f2(p1 string, p2 string) {
	fmt.Println("F2 dois parâmetros e não retorna nada")
	fmt.Printf("p1 = %s p2 = %s\n", p1, p2)
}

func f3() string {
	fmt.Println("F3 não recebe nada e renorna uma string")
	return "F3"
}

func f4(p1, p2 string) string {
	fmt.Println("F4 recebe duas strings como parâmetro e retorna outra string")
	return fmt.Sprintf("%s %s", p1, p2)
}

func f5() (string, string) {
	fmt.Println("F5 retorna duas strings")
	return "r1", "r2"
}

func fp(p string) string {
	return "FP é uma função pura que recebeu " + p + " como parâmetro"
}

func main() {
	f1()
	f2("P1", "P2")
	f3()
	f4("param1", "param2")
	f5()
	fmt.Println(fp("VALOR-STRING"))
}
