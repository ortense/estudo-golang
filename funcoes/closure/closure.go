package main

import "fmt"

func makePrint(prefix string) func(msg ...interface{}) {
	return func(msgs ...interface{}) {
		fmt.Print(prefix + " ")
		fmt.Println(msgs...)
	}
}

func main() {
	fmt.Println("Misturando Closure com variáticas que recebem slices")

	print := makePrint(">>>")
	print(1, 2, 3)
	makePrint("-->")(4, 5, 6)
}
