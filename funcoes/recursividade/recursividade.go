package main

import (
	"fmt"
)

func fatorial(n int) (int, error) {
	switch {
	case n < 0:
		return -1, fmt.Errorf("%d é um número inválido", n)
	case n == 0:
		return 1, nil
	default:
		fatorialAnterior, _ := fatorial(n - 1)
		return n * fatorialAnterior, nil
	}
}

func main() {
	r, _ := fatorial(5)
	fmt.Println(r)

	_, err := fatorial(-5)
	if err != nil {
		fmt.Println(err)
	}
}
