# comandos relacionado a testes em Go

# o parametro "./..." serve para executar todos os testes de todas as pastas
# se for rodar apenas os testes da pasta, por exemplo em testes/matematica poderia ser removido

echo -e "\n=== RODANDO TESTES ===\n"
go test ./... -v

echo -e "\n=== RODANDO TESTES COM COBERTURA ===\n"
go test ./... -cover

echo -e "\n=== RODANDO TESTE E SALVANDO PROFILE DE COBERTURA ===\n"
go test ./... -coverprofile=coverage.out

echo -e "\n=== LENDO ARQUIVO DE PROFILE ===\n"
go tool cover -func=coverage.out

echo -e "\n=== GERANDO REPORT EM HTML NO TEMP ===\n"
go tool cover -html=coverage.out